<?php
namespace Student\Database;

use Student\Model\StudentModel as StudentModel;

class Database
{
    private $connection;
    
    public function __construct()
    {   
       $servername = "localhost";
        $username = "vagrant";
        $password = "";
        try {
            $this->connection = new \PDO("mysql:host=$servername;dbname=Student", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return true;
        
         }catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    
    public function get($input)
    {
        $queryString    = $input['dataQuery'];
        $data           = $input['placeholder'];
        $queryStatement = $this->connection->prepare($queryString);
        $queryStatement->execute($data);
        $resultset = $queryStatement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultset;
    }
    
    public function post($input)
    {
        $queryString    = $input['dataQuery'];
        $data           = $input['placeholder'];
        $queryStatement = $this->connection->prepare($queryString);
        $queryStatement->execute($data);
        return array(
            'rowCount' => $queryStatement->rowCount(),
            'lastInsertId' => $this->connection->lastInsertId()
        );
    }
    
    public function update($input)
    {
        $queryString    = $input['dataQuery'];
        $data           = $input['placeholder'];
        $queryStatement = $this->connection->prepare($queryString);
        $queryStatement->execute($data);
        $affectedRows   = $queryStatement->rowCount();
        return $affectedRows;
    }
    
    public function close()
    {
        $this->connection = null;
    }
}
