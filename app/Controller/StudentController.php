<?php

namespace Student\Controller;

use Student\Model\StudentModel;
use Student\Mapper\StudentMapper;
use Student\View\StudentCreationFailed;
use Student\View\StudentCreatedSuccessfully;

/**
*
*/
class StudentController
{
    
    public function create()
    {
        $StudentModel=new StudentModel();
        $StudentModel->setRollNumber($_POST['rollNumber']);
        $StudentModel->setName($_POST['name']);
        $StudentModel->setCourse($_POST['course']);

        echo $StudentModel->getCourse();
        echo $StudentModel->getName();
        echo $StudentModel->getRollNumber();
       // write code to store in database
        $StudentMapper= new StudentMapper();
        $result=$StudentMapper->create($StudentModel);
        

        if ($result==true) {
            $view=new StudentCreatedSuccessfully();
            echo $view->render($result);
        } else {
            $view=new StudentCreationFailed();
            echo $view->render($result);
        }

    }
}
