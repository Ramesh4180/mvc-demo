<?php

namespace Student\Model;

/**
*
*/
class StudentModel
{
    protected $rollNumber=0;
    protected $name="";
    protected $course="";


    public function setRollNumber($rollNumber)
    {
        if ($this->validateRollNumber($rollNumber)==true) {
            $this->rollNumber=$rollNumber;
        }
    }

    public function setName($name)
    {
        if ($this->validate($name)==true) {
            $this->name=$name;
        }
    }

    public function setCourse($course)
    {
        if ($this->validate($course)==true) {
            $this->course=$course;
        }
    }

    public function getRollNumber()
    {
        return $this->rollNumber;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getCourse()
    {
        return $this->course;
    }

    private function validateRollNumber($num1)
    {
        if (is_numeric($num1)) {
            return true;
        } else {
            throw new \InvalidArgumentException("Input type is not integer", 1);
        }
    }

    private function validate($name)
    {
        if (is_string($name)) {
            return true;
        } else {
            throw new \InvalidArgumentException("Input type is not String", 1);
        }
    }
}
