<?php

namespace Student\View;

/**
*
*/
class StudentCreationFailed
{
    public function render($result)
    {
        return "Student creation failed".$result;
    }
}
