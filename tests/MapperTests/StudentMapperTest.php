<?php
namespace Tests\MapperTests;

use Student\Mapper\StudentMapper as StudentMapper;
use Student\Model\StudentModel as StudentModel;

class StudentMapperTest extends \PHPUnit_Extensions_Database_TestCase
{
    private $connection;
    
    public function getConnection()
    {
        
        $dbHost     = "localhost";
        $dbName     = "Student";
        $hostString = "mysql:host=$dbHost;dbname=$dbName";

        $username = "vagrant";
        $password = "";
        
        try {
            $this->connection = new \PDO($hostString,$username,$password );
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->connection->exec("set foreign_key_checks=0");
            return $this->createDefaultDBConnection($this->connection, $dbName);
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        
    }
    
    public function getDataSet()
    {
        return $this->createXMLDataSet(dirname(__FILE__) . '/_files/student_seed.xml');
    }
     public function testCanInsertRecord()
    {
        $StudentModel=new StudentModel();
        $StudentModel->setRollNumber(103);
        $StudentModel->setName('Raj');
        $StudentModel->setCourse('MSC-III');

       // write code to store in database
        $StudentMapper= new StudentMapper();
        $resultset=$StudentMapper->create($StudentModel);     
        $expectedDataSet = $this->createXmlDataSet(dirname(__FILE__) . '/_files/student_after_insert.xml');
        $actualDataSet   = $this->getConnection()->createDataSet(array(
            'Student'
        ));
        $this->assertEquals(103, $StudentModel->getRollNumber());
        $this->assertEquals('Raj', $StudentModel->getName());
        $this->assertEquals('MSC-III', $StudentModel->getCourse());

        $this->assertDataSetsEqual($expectedDataSet, $actualDataSet);
        
    }
}