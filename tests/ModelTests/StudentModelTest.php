<?php

namespace Tests\ModelTests;

class StudentModelTest extends \PHPUnit_Framework_TestCase
{
    public function testCanSetAndGetNameOfStudent()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setName("Rakesh");
        $this->assertEquals("Rakesh", $studentModel->getName());
        
    }
    public function testCanSetAndGetCourseOfStudent()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setCourse("MCS");
        $this->assertEquals("MCS", $studentModel->getCourse());
        
    }
    public function testCanSetAndGetRollNoOfStudent()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setRollNumber(10);
        $this->assertEquals(10, $studentModel->getRollNumber());
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotNumber()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setRollNumber("one");  
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setRollNumber(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputNameIsNull()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setName(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputNameIsNotString()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setName(1);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputCourseIsNull()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setName(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputCourseIsNotString()
    {
        $studentModel = new \Student\Model\StudentModel();
        $studentModel->setName(1);
    }

}
