<?php

namespace Tests\ModelTests;

class StudenCreationFailedTest extends \PHPUnit_Framework_TestCase
{
    public function testCanGetFailedViewOfStudent()
    {
        $studentView = new \Student\View\StudentCreationFailed;
        $this->assertEquals("Student creation failedFailed", $studentView->render("Failed"));
    }
}
