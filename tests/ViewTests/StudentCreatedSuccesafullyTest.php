<?php

namespace Tests\ModelTests;

class StudenCreatedSuccessfullyTest extends \PHPUnit_Framework_TestCase
{
    public function testCanGetSuccessViewOfStudent()
    {
        $studentView = new \Student\View\StudentCreatedSuccessfully;
        $this->assertEquals("Student created successfullyOK", $studentView->render("OK"));
    }
}
